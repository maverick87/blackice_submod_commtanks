leader_traits = {
#Based on HoI3 BICE traits by Riever
#Adapted for HOI4 1.5 by maverick87
##################################
# Table of Contents
# - Personality Traits Land / Air
# - Personality Traits Navy
##################################
	old_guard = { 
		type = land
		trait_type = personality_trait
		
		modifier = {
			max_dig_in = 1
			army_morale_factor = -0.05
		}
		
		non_shared_modifier = {
			experience_gain_factor = -0.25
		}
	}
	
	basic_hq = { 
		type = land
		trait_type = personality_trait
		new_commander_weight = {
			factor = 0  # Scripted starting leaders only
		}		
		field_marshal_modifier = {
			max_army_group_size = 1
		}
		non_shared_modifier = {
			max_commander_army_size = -2
			
			hidden_modifier = {
				reassignment_duration_factor = 14
			}
		}
		
	}
	
	interservice_hq = { 
		type = land
		trait_type = personality_trait
		new_commander_weight = {
			factor = 0  # Scripted starting leaders only
		}		
		field_marshal_modifier = {
			max_army_group_size = 1
		}	
		non_shared_modifier = {
			max_commander_army_size = -1
			
			hidden_modifier = {
				reassignment_duration_factor = 14
			}
		}
		
	}

	brigade_hq = { 
		type = land
		trait_type = personality_trait
		new_commander_weight = {
			factor = 0  # Scripted starting leaders only
		}	
		field_marshal_modifier = {
			max_army_group_size = 1
		}		
		non_shared_modifier = {
			max_commander_army_size = 0
			hidden_modifier = {
				reassignment_duration_factor = 14
			}
		}
		
	}

	corps_hq = { 
		type = land
		trait_type = personality_trait
		new_commander_weight = {
			factor = 0  # Scripted starting leaders only
		}		
		field_marshal_modifier = {
			max_army_group_size = 1
		}	
		non_shared_modifier = {
			max_commander_army_size = 1
			hidden_modifier = {
				reassignment_duration_factor = 14
			}
		}
		
	}

	army_hq = { 
		type = land
		trait_type = personality_trait
		new_commander_weight = {
			factor = 0  # Scripted starting leaders only
		}		
		field_marshal_modifier = {
			max_army_group_size = 1
		}	
		non_shared_modifier = {
			max_commander_army_size = 2
			hidden_modifier = {
				reassignment_duration_factor = 14
			}
		}
		
	}

	army_group_hq = { 
		type = land
		trait_type = personality_trait
		new_commander_weight = {
			factor = 0  # Scripted starting leaders only
		}			
		field_marshal_modifier = {
			max_army_group_size = 2
		}	
		non_shared_modifier = {
			max_commander_army_size = 2
			hidden_modifier = {
				reassignment_duration_factor = 14
			}
		}		
	}

	brilliant_strategist = { 
		type = land
		trait_type = personality_trait
		
		attack_skill = 1
		planning_skill = 1
		
		non_shared_modifier = {	
			army_attack_skill_chance = 0.2
			planning_skill_chance = 0.2
		}
		
	}

	inflexible_strategist = { 
		type = land
		trait_type = personality_trait
		
		defense_skill = 1
		logistics_skill = 1
		
		non_shared_modifier = {	
			army_defense_skill_chance = 0.2
			logistics_skill_chance = 0.2
		}
	}

	politically_connected = {
		type = { land navy }
		trait_type = personality_trait
		
		non_shared_modifier = {
			experience_gain_factor = -0.1
			promote_cost_factor = -0.2
			planning_skill_chance = 0.2
			logistics_skill_chance = 0.2
		}
		
	}

	war_hero = { 
		type = { land navy }
		trait_type = personality_trait
		
		non_shared_modifier = {
			promote_cost_factor = -0.3
			reassignment_duration_factor = 7   # +50% cost to replace leader
			army_attack_skill_chance = 0.2
			planning_skill_chance = 0.2
		}
		
		new_commander_weight = {
			factor = 0  # Scripted starting leaders only
		}
	}

	career_officer = { 
		type = land
		trait_type = personality_trait
		
		non_shared_modifier = {
			promote_cost_factor = -0.15
			planning_skill_chance = 0.2
			logistics_skill_chance = 0.2
		}
		
	}

	trait_cautious = { 
		type = land
		trait_type = personality_trait
		
		modifier = {
			max_planning_factor = 0.1
			planning_speed = -0.20
            wounded_chance_factor = -0.4
		}
		non_shared_modifier = {
			army_defense_skill_chance = 0.2
			logistics_skill_chance = 0.2
		}
		
	}

	trait_reckless = {
		type = land
		trait_type = personality_trait
		
		modifier = {
			max_planning_factor = -0.2
			planning_speed = 0.1
			wounded_chance_factor = 0.5  # +50% chance to get wounded
		}
		
		non_shared_modifier = {
			army_attack_skill_chance = 0.2
			army_defense_skill_chance = -0.2
			logistics_skill_chance = -0.2
		}
	}

	media_personality = { 
		type = land
		trait_type = personality_trait
		
		non_shared_modifier = {
			reassignment_duration_factor = 14   # +100% cost to replace leader
			army_attack_skill_chance = 0.2
			army_defense_skill_chance = 0.2
		}
		
	}

	harsh_leader = { 
		type = land
		trait_type = personality_trait
		
		attack_skill = 1

		modifier = {
			army_morale_factor = -0.10
			army_attack_skill_chance = 0.2
			logistics_skill_chance = 0.2
		}
		
	}

	bearer_of_artillery = {
		type = corps_commander
		
		trait_type = personality_trait
		
		modifier = {
			army_artillery_attack_factor = 0.05
		}
		
		new_commander_weight = {
			factor = 0
		}
	}
	
	infantry_officer = { 
		type = land
		trait_type = personality_trait
		
		trait_xp_factor = {
			infantry_leader = 0.5     #+50%
		}
		
		non_shared_modifier = {
			army_defense_skill_chance = 0.2
			planning_skill_chance = 0.2
		}
	}
	
	cavalry_officer = { 
		type = land
		trait_type = personality_trait
		
		trait_xp_factor = {
			cavalry_leader = 0.5     #+50%
		}
		
		non_shared_modifier = {
			army_attack_skill_chance = 0.2
			logistics_skill_chance = 0.2
		}
	}
	
	armor_officer = { 
		type = land
		trait_type = personality_trait
		
		trait_xp_factor = {
			panzer_leader = 0.5		#+550%
		}
		
		non_shared_modifier = {
			army_attack_skill_chance = 0.2
			planning_skill_chance = 0.2
		}
	}

	gung_ho_general = { # LEEEEEEEEEEROY JENKINS!!!
		type = land
		trait_type = personality_trait
		
		allowed = {
			always = no
		}
		
		modifier = {
			offence = 0.05
			attrition = 0.1
			supply_consumption_factor = 0.05
			land_reinforce_rate = 0.05
		}
		
		ai_will_do = {
			factor = 1
		}
	}

	bad_staffer = { 
		type = land
		trait_type = personality_trait
		allowed = {
			always = no
		}	
		modifier = {
			supply_consumption_factor = 0.02
			planning_speed = -0.1
			recon_factor = -0.1
		}
		corps_commander_modifier = {
			max_commander_army_size = -2
			max_army_group_size = -1
		}
	}
	
	desert_1 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }		
		modifier = {
			desert = {
				movement = 0.01
				attack = 0.015
				defence = 0.015
			}
		}
	}
	desert_2 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.02
				attack = 0.03
				defence = 0.03
			}
		}
	}
	desert_3 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.03
				attack = 0.045
				defence = 0.045
			}
		}
	}
	desert_4 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.04
				attack = 0.06
				defence = 0.06
			}
		}
	}
	desert_5 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.05
				attack = 0.075
				defence = 0.075
			}
		}
	}
	desert_6 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.06
				attack = 0.09
				defence = 0.09
			}
		}
	}
	desert_7 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.07
				attack = 0.105
				defence = 0.105
			}
		}
	}
	desert_8 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.08
				attack = 0.12
				defence = 0.12
			}
		}
	}
	desert_9 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.09
				attack = 0.135
				defence = 0.135
			}
		}
	}
	desert_10 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			desert = {
				movement = 0.1
				attack = 0.15
				defence = 0.15
			}
		}
	}
	
	
	hills_1 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }		
		modifier = {
			hills = {
				movement = 0.01
				attack = 0.015
				defence = 0.015
			}
		}
	}
	hills_2 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.02
				attack = 0.03
				defence = 0.03
			}
		}
	}
	hills_3 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.03
				attack = 0.045
				defence = 0.045
			}
		}
	}
	hills_4 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.04
				attack = 0.06
				defence = 0.06
			}
		}
	}
	hills_5 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.05
				attack = 0.075
				defence = 0.075
			}
		}
	}
	hills_6 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.06
				attack = 0.09
				defence = 0.09
			}
		}
	}
	hills_7 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.07
				attack = 0.105
				defence = 0.105
			}
		}
	}
	hills_8 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.08
				attack = 0.12
				defence = 0.12
			}
		}
	}
	hills_9 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.09
				attack = 0.135
				defence = 0.135
			}
		}
	}
	hills_10 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			hills = {
				movement = 0.1
				attack = 0.15
				defence = 0.15
			}
		}
	}
	
	
	mountain_1 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }		
		modifier = {
			mountain = {
				movement = 0.01
				attack = 0.015
				defence = 0.015
			}
		}
	}
	mountain_2 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.02
				attack = 0.03
				defence = 0.03
			}
		}
	}
	mountain_3 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.03
				attack = 0.045
				defence = 0.045
			}
		}
	}
	mountain_4 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.04
				attack = 0.06
				defence = 0.06
			}
		}
	}
	mountain_5 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.05
				attack = 0.075
				defence = 0.075
			}
		}
	}
	mountain_6 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.06
				attack = 0.09
				defence = 0.09
			}
		}
	}
	mountain_7 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.07
				attack = 0.105
				defence = 0.105
			}
		}
	}
	mountain_8 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.08
				attack = 0.12
				defence = 0.12
			}
		}
	}
	mountain_9 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.09
				attack = 0.135
				defence = 0.135
			}
		}
	}
	mountain_10 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			mountain = {
				movement = 0.1
				attack = 0.15
				defence = 0.15
			}
		}
	}
	
	
	urban_1 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }		
		modifier = {
			urban = {
				movement = 0.01
				attack = 0.015
				defence = 0.015
			}
		}
	}
	urban_2 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.02
				attack = 0.03
				defence = 0.03
			}
		}
	}
	urban_3 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.03
				attack = 0.045
				defence = 0.045
			}
		}
	}
	urban_4 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.04
				attack = 0.06
				defence = 0.06
			}
		}
	}
	urban_5 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.05
				attack = 0.075
				defence = 0.075
			}
		}
	}
	urban_6 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.06
				attack = 0.09
				defence = 0.09
			}
		}
	}
	urban_7 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.07
				attack = 0.105
				defence = 0.105
			}
		}
	}
	urban_8 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.08
				attack = 0.12
				defence = 0.12
			}
		}
	}
	urban_9 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.09
				attack = 0.135
				defence = 0.135
			}
		}
	}
	urban_10 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			urban = {
				movement = 0.1
				attack = 0.15
				defence = 0.15
			}
		}
	}
	
	
	forest_1 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }		
		modifier = {
			forest = {
				movement = 0.01
				attack = 0.015
				defence = 0.015
			}
		}
	}
	forest_2 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.02
				attack = 0.03
				defence = 0.03
			}
		}
	}
	forest_3 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.03
				attack = 0.045
				defence = 0.045
			}
		}
	}
	forest_4 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.04
				attack = 0.06
				defence = 0.06
			}
		}
	}
	forest_5 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.05
				attack = 0.075
				defence = 0.075
			}
		}
	}
	forest_6 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.06
				attack = 0.09
				defence = 0.09
			}
		}
	}
	forest_7 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.07
				attack = 0.105
				defence = 0.105
			}
		}
	}
	forest_8 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.08
				attack = 0.12
				defence = 0.12
			}
		}
	}
	forest_9 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.09
				attack = 0.135
				defence = 0.135
			}
		}
	}
	forest_10 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			forest = {
				movement = 0.1
				attack = 0.15
				defence = 0.15
			}
		}
	}
	
	
	
	jungle_1 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }		
		modifier = {
			jungle = {
				movement = 0.01
				attack = 0.015
				defence = 0.015
			}
		}
	}
	jungle_2 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.02
				attack = 0.03
				defence = 0.03
			}
		}
	}
	jungle_3 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.03
				attack = 0.045
				defence = 0.045
			}
		}
	}
	jungle_4 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.04
				attack = 0.06
				defence = 0.06
			}
		}
	}
	jungle_5 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.05
				attack = 0.075
				defence = 0.075
			}
		}
	}
	jungle_6 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.06
				attack = 0.09
				defence = 0.09
			}
		}
	}
	jungle_7 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.07
				attack = 0.105
				defence = 0.105
			}
		}
	}
	jungle_8 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.08
				attack = 0.12
				defence = 0.12
			}
		}
	}
	jungle_9 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.09
				attack = 0.135
				defence = 0.135
			}
		}
	}
	jungle_10 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			jungle = {
				movement = 0.1
				attack = 0.15
				defence = 0.15
			}
		}
	}
	
	
	
	swamp_1 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }		
		modifier = {
			marsh = {
				movement = 0.01
				attack = 0.015
				defence = 0.015
			}
		}
	}
	swamp_2 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.02
				attack = 0.03
				defence = 0.03
			}
		}
	}
	swamp_3 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.03
				attack = 0.045
				defence = 0.045
			}
		}
	}
	swamp_4 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.04
				attack = 0.06
				defence = 0.06
			}
		}
	}
	swamp_5 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.05
				attack = 0.075
				defence = 0.075
			}
		}
	}
	swamp_6 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.06
				attack = 0.09
				defence = 0.09
			}
		}
	}
	swamp_7 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.07
				attack = 0.105
				defence = 0.105
			}
		}
	}
	swamp_8 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.08
				attack = 0.12
				defence = 0.12
			}
		}
	}
	swamp_9 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.09
				attack = 0.135
				defence = 0.135
			}
		}
	}
	swamp_10 = { 
 		type = corps_commander
		trait_type = personality_trait
		new_commander_weight = { factor = 0 }
		modifier = {
			marsh = {
				movement = 0.1
				attack = 0.15
				defence = 0.15
			}
		}
	}
		
	naval_training_officer = {
		type = navy
		trait_type = personality_trait
		
		modifier = {
			training_time_army_factor = -0.10
			experience_gain_navy = 0.01
		}
	}
	naval_training_officer_2 = {
		type = navy
		trait_type = personality_trait
		
		modifier = {
			training_time_army_factor = -0.10
			experience_gain_navy = 0.02
		}
	}
	air_training_officer = { 
		type = land
		trait_type = personality_trait
		
		modifier = {
			training_time_army_factor = -0.10
			experience_gain_air = 0.01
		}
	}
	air_training_officer_2 = {
		type = land
		trait_type = personality_trait
		
		modifier = {
			training_time_army_factor = -0.10
			experience_gain_air = 0.02
		}
	}
	training_officer = { 
		type = land
		trait_type = personality_trait
		
		modifier = {
			training_time_army_factor = -0.10
			experience_gain_army = 0.01
		}
	}
	training_officer_2 = {
		type = land
		trait_type = personality_trait
		
		gain_xp = {
			has_trait = training_officer
			check_variable = { player = 1 }
			check_variable = { num_units > 4 }
			skill > 1
		}
		cost = 1000
		modifier = {
			training_time_army_factor = -0.05
			experience_gain_army = 0.02
		}
	}
}
### EOF ###